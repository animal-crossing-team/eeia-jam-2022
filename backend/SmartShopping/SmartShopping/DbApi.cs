﻿using MySql.Data;
using MySql.Data.MySqlClient;

namespace SmartShopping;

public class DbApi
{
    private DbApi(){}

    ~DbApi() => Close();
    public string Server { get; set; }
    public string DbName { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }

    public MySqlConnection Connection { get; set; }
    private static DbApi? _instance = null;

    public static DbApi Instance()
    {
        if (_instance == null)
            _instance = new DbApi();
        return _instance;
    }
    public bool IsConnect()
    {
        if (Connection != null) return true;
        if (string.IsNullOrEmpty(DbName))
            return false;
        var connstring = string.Format("Server={0}; database={1}; UID={2}; password={3}", Server, DbName, UserName, Password);
        Connection = new MySqlConnection(connstring);
        Connection.Open();

        return true;
    }
    
    public void Close()
    {
        Connection.Close();
    }        
}