﻿using MySql.Data.MySqlClient;
using SmartShopping.Models;
using MySqlConnection = MySqlConnector.MySqlConnection;

namespace SmartShopping;

public static class DbQuery
{
    private static readonly DbApi DbCon;
    static DbQuery()
    {
        DbCon = DbApi.Instance();
        DbCon.Server = "25.51.45.198";
        DbCon.DbName = "smart_shopping";
        DbCon.UserName = "admin";
        DbCon.Password = "admin";
    }

    public static void Disconnect() => DbCon?.Close();

    private static bool? IsLoginTaken(string login)
    {
        if (!DbCon.IsConnect()) return null;
        var query = "SELECT id, name FROM ss_user WHERE name= '" + login + "'";
        var cmd = new MySqlCommand(query, DbCon?.Connection);
        var reader = cmd.ExecuteReader();
        while(reader.Read())
        {
            reader.Close();
            return true;
        }
        reader.Close();
        return false;
    }
    public static User? GetUser(string login, string password)
    {
        if (!DbCon.IsConnect()) return null;
        User user = null;
        var query = "SELECT id, name FROM ss_user WHERE name= '" + login + "' AND password= '" + password+"'";
        var cmd = new MySqlCommand(query, DbCon?.Connection);
        var reader = cmd.ExecuteReader();
        if(reader.Read())
        {
            user = new();
            user.Id = reader.GetInt32(0);
            user.Name = reader.GetString(1);
            reader.Close();
            user.ConsumedProducts = GetUserConsumedProducts(user.Id);
            user.OwnedProducts = GetUserOwnedProducts(user.Id);
            user.ShoppingList = GetUserShoppingList(user.Id);
        }
        else
            reader.Close();

        return user;
    }

    public static bool AddUser(string? login, string? password)
    {
        if (!DbCon.IsConnect()||login is null  || password is null) return false;
        var exist = IsLoginTaken(login);
        if (exist is true or null) return false;
        const string query = "INSERT INTO ss_user(name,password) VALUES(@login,@password);";
        var cmd = new MySqlCommand(query, DbCon.Connection);
        cmd.Parameters.AddWithValue("@login", login);
        cmd.Parameters.AddWithValue("@password", password);
        cmd.ExecuteNonQuery();
        return true;
    }

  

    private static bool? IsProductExist(string barcode)
    {
        if (!DbCon.IsConnect()) return null;
        var query = "SELECT id FROM ss_products WHERE barcode= '" + barcode + "'";
        var cmd = new MySqlCommand(query, DbCon?.Connection);
        var reader = cmd.ExecuteReader();
        while(reader.Read())
        {
            reader.Close();
            return true;
        }
        reader.Close();
        return false;
    }
    public static bool AddProduct(Product p)
    {
        if (!DbCon.IsConnect()) return false;
        if (p.Barcode != null)
        {
            var exist = IsProductExist(p.Barcode);
            if (exist is true or null) return false;
        }

        const string query = "INSERT INTO ss_products(name,barcode,img_url,waste_type,product_type) VALUES(@name,@barcode,@img_url,@waste_type,@product_type);";
        var cmd = new MySqlCommand(query, DbCon.Connection);
        cmd.Parameters.AddWithValue("@name",p.Name ??= string.Empty);
        cmd.Parameters.AddWithValue("@barcode",p.Barcode ??= string.Empty);
        cmd.Parameters.AddWithValue("@img_url",p.ImgUrl ??= string.Empty);
        int w = 1;
        if (p.WasteType is not null) w = p.WasteType.Id; 
        cmd.Parameters.AddWithValue("@waste_type", w);
        w = 1;
        if (p.ProductType is not null) w = p.ProductType.Id;
        cmd.Parameters.AddWithValue("@product_type", w);
        cmd.ExecuteNonQuery();
        return true;
    }
    
    
    public static Tuple<List<int>, List<int>?, List<DateTime>> GetConsumedProductDataByUserId(int id, string dbName)
    {
        if (!DbCon.IsConnect()) return null;
        var productInfo = new List<int>();
        var resQuant = new List<int>();
        var resDatatime = new List<DateTime>();
                          var query = "SELECT product_id, quantity, datetime FROM "+dbName+" WHERE user_id="+id;
        var cmd = new MySqlCommand(query, DbCon.Connection);
        var reader = cmd.ExecuteReader();
        while(reader.Read())
        {
            productInfo.Add(reader.GetInt32(0));
            resQuant.Add(reader.GetInt32(1));
            resDatatime.Add(reader.GetDateTime(2));
        }
        reader.Close();
        return new Tuple<List<int>, List<int>?,List<DateTime>>(productInfo, resQuant, resDatatime);
    }

    public static List<ConsumedProduct>? GetUserConsumedProducts(int id)
    {
        List<int> productId;
        List<int>? resQuant;
        List<DateTime> resDateTimes;
        (productId, resQuant,resDateTimes) = GetConsumedProductDataByUserId(id,"ss_products_consumption");
        var resProducts = GetProductsListFromIntegerList(productId);
        if (resProducts == null|| resQuant == null) return null;
        var resultList = new List<ConsumedProduct>();
        for (var i = 0; i < resProducts.Count; i++)
        {
            var cp = new ConsumedProduct();
            cp.Product = resProducts[i];
            cp.Product.Quantity = resQuant[i];
            cp.DateTime = resDateTimes[i];
            resultList.Add(cp);
        }
        return resultList;
    }
    
    public static Tuple<List<int>, List<int>?>? GetProductIdAndQuantityByUserId(int id, string dbName)
    {
        if (!DbCon.IsConnect()) return null;
        var productInfo = new List<int>();
        var resQuant = new List<int>();
        var query = "SELECT product_id, quantity FROM "+dbName+" WHERE user_id="+id;
        var cmd = new MySqlCommand(query, DbCon.Connection);
        var reader = cmd.ExecuteReader();
        while(reader.Read())
        {
            productInfo.Add(reader.GetInt32(0));
            resQuant.Add(reader.GetInt32(1));
        }
        reader.Close();
        return new Tuple<List<int>, List<int>?>(productInfo, resQuant);
    }

    public static Product? GetProduct(int id)
    {
        if (!DbCon.IsConnect()) return null;
        var query =
            "SELECT p.id,p.name,p.barcode,p.img_url,w.id,w.name_pl,w.name_eng,pt.id,pt.name FROM ss_products p LEFT JOIN ss_products_waste_types w ON p.waste_type = w.id LEFT JOIN ss_products_types pt on p.product_type = pt.id WHERE p.id = "+id;
        var cmd = new MySqlCommand(query, DbCon.Connection);
        var reader = cmd.ExecuteReader();
        var p = new Product();
        while(reader.Read())
        {
            var w = new WasteType();
            var pt = new ProductTypes();
            p.Id = reader.GetInt32(0);
            p.Name = reader.GetString(1);
            p.Barcode = reader.GetString(2);
            p.ImgUrl = reader.GetString(3);
            w.Id = reader.GetInt32(4);
            w.NamePl = reader.GetString(5);
            w.NameEng = reader.GetString(6);
            pt.Id = reader.GetInt32(7);
            pt.Name = reader.GetString(8);
            p.WasteType = w;
            p.ProductType = pt;
        }
        p.Ingredients = new List<Ingredient>();
        p.NutritionalValues = new List<NutritionalValue>();
        reader.Close();
        return p;
    }
    
    public static List<Product>? GetProductsListFromIntegerList(List<int> productId)
    {
        return productId.Select(pid => GetProduct(pid)!).ToList();
    }
    public static List<Product>? GetUserShoppingList(int id)
    {
        List<int> productId;
        List<int>? resQuant;
        (productId, resQuant) = GetProductIdAndQuantityByUserId(id,"ss_shopping_list");
        var resProducts = GetProductsListFromIntegerList(productId);
        if (resProducts == null) return null;
        for(var i =0;i<resProducts.Count;i++)
        {
            resProducts[i].Quantity = resQuant[i];
        }

        return resProducts;
    }
    public static List<Product>? GetUserOwnedProducts(int id)
    {
        List<int> productId;
        List<int>? resQuant;
        (productId, resQuant) = GetProductIdAndQuantityByUserId(id,"ss_products_owned_by_user");
        var resProducts = GetProductsListFromIntegerList(productId);
        if (resProducts == null) return null;
        for(var i =0;i<resProducts.Count;i++)
        {
            resProducts[i].Quantity = resQuant[i];
        }

        return resProducts;
    }

    public static List<NutritionalValue> GetNutritionalValueByProduct(int productId)
    {
        if (!DbCon.IsConnect()) return null;
        var nutritionalValueList = new List<NutritionalValue>();
        var query =
            "SELECT nv.id, nv.name,q_per_100,q_per_package FROM ss_products_nutritional_values_in_products LEFT JOIN ss_products_nutritional_values as nv on nv_id = nv.id WHERE product_id=" +
            productId;
        var cmd = new MySqlCommand(query, DbCon.Connection);
        var reader = cmd.ExecuteReader();
        while(reader.Read())
        {
            var nutritionalValue = new NutritionalValue();
            nutritionalValue.Id = reader.GetInt32(0);
            nutritionalValue.Name = reader.GetString(1);
            nutritionalValue.Per100g = reader.GetDouble(2);
            nutritionalValue.PerPackage = reader.GetDouble(3);
            nutritionalValueList.Add(nutritionalValue);
        }
        reader.Close();
        return nutritionalValueList;

    }
    

    public static bool? SetQuantityOfProducts(int id, int no, int userId)
    {
        if (!DbCon.IsConnect()) return false;
        var query = $"UPDATE ss_products_owned_by_user SET quantity = {no} WHERE id = {id} AND user_id = {userId}";
        var cmd = new MySqlCommand(query, DbCon.Connection);
        cmd.ExecuteNonQuery();
        return true;
    }

    public static bool RemoveProduct(int id, int userId)
    {
        if (!DbCon.IsConnect()) return false;
        var query = "DELETE FROM ss_products_owned_by_user WHERE user_id="+userId+" AND product_id="+id+";";
        var cmd = new MySqlCommand(query, DbCon.Connection);
        cmd.ExecuteNonQuery();
        return true;
    }

    public static bool AddProduct(int id, int no, int userId)
    {
        if (!DbCon.IsConnect()) return false;
        var query = "INSERT INTO ss_products_owned_by_user(user_id,product_id,quantity) VALUES (@uid,@pid,@q)";
        var cmd = new MySqlCommand(query, DbCon.Connection);
        cmd.Parameters.AddWithValue("@uid", userId);
        cmd.Parameters.AddWithValue("@pid", id);
        cmd.Parameters.AddWithValue("@q", no);
        cmd.ExecuteNonQuery();
        return true;
    }
    
    public static bool AddProductSl(int id, int no, int userId)
    {
        if (!DbCon.IsConnect()) return false;
        var query = "INSERT INTO ss_shopping_list(user_id,product_id,quantity) VALUES (@uid,@pid,@q)";
        var cmd = new MySqlCommand(query, DbCon.Connection);
        cmd.Parameters.AddWithValue("@uid", userId);
        cmd.Parameters.AddWithValue("@pid", id);
        cmd.Parameters.AddWithValue("@q", no);
        cmd.ExecuteNonQuery();
        return true;
    }
    
    public static Product? GetProductFromDB(string barcode)
    {
       if(!DbCon.IsConnect()) return null;
       var query = "SELECT p.id,p.name,p.barcode,p.img_url,w.id,w.name_pl,w.name_eng,pt.id,pt.name FROM ss_products p LEFT JOIN ss_products_waste_types w ON p.waste_type = w.id LEFT JOIN ss_products_types pt on p.product_type = pt.id WHERE p.barcode = '"+barcode+"';";
       var cmd = new MySqlCommand(query, DbCon.Connection);
       var reader = cmd.ExecuteReader();
       var p = new Product();
       while(reader.Read())
       {
           var w = new WasteType();
           var pt = new ProductTypes();
           p.Id = reader.GetInt32(0);
           p.Name = reader.GetString(1);
           p.Barcode = reader.GetString(2);
           p.ImgUrl = reader.GetString(3);
           w.Id = reader.GetInt32(4);
           w.NamePl = reader.GetString(5);
           w.NameEng = reader.GetString(6);
           pt.Id = reader.GetInt32(7);
           pt.Name = reader.GetString(8);
           p.WasteType = w;
           p.ProductType = pt;
       }
       p.Ingredients = new List<Ingredient>();
       p.NutritionalValues = new List<NutritionalValue>();
       reader.Close();
       return p;
    }
}
