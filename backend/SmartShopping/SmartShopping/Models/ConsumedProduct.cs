﻿namespace SmartShopping.Models;

public record ConsumedProduct
{
    public Product Product { get; set; }
    public DateTime DateTime { get; set; }
}