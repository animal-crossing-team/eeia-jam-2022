namespace SmartShopping.Models;

public record FrontBack
{
	public int UserId { get; set; }
	public int? ProductId { get; set; }
	public int? Quantity { get; set; }
	public string? Barcode { get; set; }
}
