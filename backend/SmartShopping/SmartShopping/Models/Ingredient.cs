﻿namespace SmartShopping.Models;

public record Ingredient
{
    public int Id { get; set; }
    public string? Name { get; set; }
}