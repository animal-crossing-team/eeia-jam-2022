﻿namespace SmartShopping.Models;

public record NutritionalValue
{
    public int Id { get; set; }
    public string? Name { get; set; }

    public double? Per100g { get; set; }

    public double? PerPackage { get; set; }
}