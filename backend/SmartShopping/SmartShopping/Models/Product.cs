﻿using System.Text;

namespace SmartShopping.Models;

public record Product
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Barcode { get; set; }
    public string? ImgUrl { get; set; }
    public WasteType? WasteType { get; set; }
    public ProductTypes? ProductType { get; set; }
    public List<Ingredient>? Ingredients { get; set; }
    public List<NutritionalValue>? NutritionalValues { get; set; }
    public int? Quantity { get; set; }

    public override string ToString()
    {
        StringBuilder stringBuilder = new();
        stringBuilder.Append($"Id : {Id}, ");
        stringBuilder.Append($"Quantity : {Quantity}, ");
        stringBuilder.Append($"Name : {Name}, ");
        stringBuilder.Append($"Barcode : {Barcode}, ");
        stringBuilder.Append($"ImgUrl : {ImgUrl}, ");
        stringBuilder.Append($"WasteType : {WasteType}, ");
        stringBuilder.Append($"ProductType : {ProductType}, ");
        
        if (NutritionalValues == null)
            return stringBuilder.ToString();

        stringBuilder.Append("NutritionalValues : {{");

        foreach (var nutrient in NutritionalValues)
            stringBuilder.Append($"{nutrient},");

        stringBuilder.Remove(stringBuilder.Length - 1, 1);
        stringBuilder.Append("}}");
        return stringBuilder.ToString();
    }
}