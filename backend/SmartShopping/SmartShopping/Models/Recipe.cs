﻿namespace SmartShopping.Models;

public record Recipe
{
	public int Id { get; set; }
	public string? Name { get; set; }
	public string? Description { get; set; }
	public string? Author { get; set; }
	public string? ImgUrl { get; set; }
	public List<Product> Products { get; set; }
}