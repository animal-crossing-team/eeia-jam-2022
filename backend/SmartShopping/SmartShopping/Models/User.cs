﻿namespace SmartShopping.Models;

public record User
{
	public int Id { get; set; }
	public string? Name { get; set; }
	
	public List<Product>? OwnedProducts { get; set; }
	public List<Product>?  ShoppingList { get; set; }
	public List<ConsumedProduct>? ConsumedProducts { get; set; }
	
	
}