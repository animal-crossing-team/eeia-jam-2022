namespace SmartShopping.Models;

record UserMinimal
{
	public string? Login { get; set; }
	public string? Password { get; set; }
}