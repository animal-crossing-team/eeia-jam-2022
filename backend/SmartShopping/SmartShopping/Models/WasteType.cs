﻿namespace SmartShopping.Models;

public record WasteType
{
	public int Id { get; set; }
	public string? NameEng { get; set; }
	public string? NamePl { get; set; }
}