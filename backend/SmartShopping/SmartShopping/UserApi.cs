﻿using SmartShopping.Models;

namespace SmartShopping;

public class UserApi
{
    public User User { get;}

    public UserApi(User user)
    {
        User = user;
    }

    public Product? GetProduct(string barcode)
    {
        return User.OwnedProducts?.Find(product => product.Barcode != null && product.Barcode.Equals(barcode));
    }

    public bool SetQuantityOfProduct(int id, int no, int userId)
    {
        var p = DbQuery.GetUserOwnedProducts(userId).Find(product => product.Id == id);
        if (p is null) return false;
        if (no == 0)
        {
            RemoveProduct(id,userId);
            return true;
        }
        else
        {
            if (DbQuery.SetQuantityOfProducts(id, no,userId) is null or false) return false;
            return true;
        }
    }

    public bool RemoveProduct(int id, int userId)
    {
        var p = DbQuery.GetUserOwnedProducts(userId).Find(product => product.Id == id);
        if (p is null) return false;
        if(!DbQuery.RemoveProduct(id,User.Id))return false;
        return true;
    }

    public int NumberOfProduct(int id,int userId)
    {
        return DbQuery.GetUserOwnedProducts(userId).Find(product => product.Id == id).Quantity ??= 0;
    }

    public bool AddProductToMagazine(int id,int no, int userId)
    {
        return no >= 0 && DbQuery.AddProduct(id,no,userId);
    }
    
    public bool AddProductToShoppingList(int id, int no,int userId)
    {
        return no>=0&& DbQuery.AddProductSl(id,no,userId);
    }

    public bool RemoveProductFromShoppingList(int id, int no)
    {
        // Todo: implement
        return false;
    }

    public List<Product> GetProductsListFromShoppingList(int userId)
    {
        return DbQuery.GetUserShoppingList(userId);
    }
}