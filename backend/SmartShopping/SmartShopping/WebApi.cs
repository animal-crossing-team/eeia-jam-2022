using Microsoft.AspNetCore.Mvc;
using SmartShopping.Models;

namespace SmartShopping;

public class WebApi
{
	private WebApplication app;

	private UserApi userApi = new UserApi(DbQuery.GetUser("Konrad","Basta"));
	private string user = "/user";
	private string magazine = "/magazine";
	private string shoppingList = "/shopping-list";
	private string recipes = "/recipes";
	private string password;
	
	public WebApi()
	{
		var builder = WebApplication.CreateBuilder();
		builder.Services.AddDatabaseDeveloperPageExceptionFilter();

		builder.Services.AddCors(options =>
		{
			options.AddPolicy("MyPolicy",
				builder =>
				{
					builder
						.AllowAnyOrigin()
						.AllowAnyMethod()
						.AllowAnyHeader();
				});
		});
		var i = new MvcOptions();
		i.EnableEndpointRouting = false;


		app = builder.Build();


		app.MapGet("/", async () => string.Empty);
		app.MapPost($"products/add", async (Product p) => DbQuery.AddProduct(p));
		//User
		app.MapPost($"{user}/login", async (UserMinimal u) => Login(u));
		app.MapPost($"{user}/registry", async (UserMinimal u) => Registry(u));

		//Magazine
		app.MapPost($"{magazine}/add", async (FrontBack fb) => AddProduct(fb));
		app.MapPost($"{magazine}/get", async (FrontBack fb) => GetProducts(fb));
		
		app.MapPost($"{magazine}/set-amount", async (FrontBack fb) => SetAmount(fb));
		app.MapPost($"{magazine}/remove", async (FrontBack fb) => RemoveProduct(fb));
		app.MapPost($"{magazine}/find", async (FrontBack fb) => FindProduct(fb));

		//Shopping List
		app.MapPost($"{shoppingList}/suggest", 
			async (FrontBack fb) => Console.WriteLine($"{magazine}/suggest Not implemented"));
		app.MapPost($"{shoppingList}/get", 
			async (FrontBack fb) => userApi!.GetProductsListFromShoppingList(fb.UserId));
		app.MapPost($"{shoppingList}/add", async (FrontBack fb) => AddProductsToShoppingList(fb));
		app.MapPost($"{shoppingList}/remove", async (FrontBack fb) => RemoveProductsFromShoppingList(fb));
		
		app.UseRouting();
		app.UseCors("MyPolicy");
		
		app.Run();
	}

	private int Login(UserMinimal user)
	{
		password = user.Password;
		user.Login ??= string.Empty;
		user.Password ??= string.Empty;
		User temp = DbQuery.GetUser(user.Login, user.Password);
		if (temp == null)
			return -1;
			
		userApi = new UserApi(temp);

		return userApi.User.Id;
	}

	private bool Registry(UserMinimal user)
	{
		user.Login ??= string.Empty;
		user.Password ??= string.Empty;
		return DbQuery.AddUser(user.Login, user.Password);
	}

	private Product? GetProduct(FrontBack? fb)
	{
		if (fb == null
		    || fb.Barcode == null)
			return null;

		Product product = userApi.GetProduct(fb.Barcode!);
		return product;
	}

	private bool SetAmount(FrontBack? fb)
	{
		if (fb == null
		    || fb.ProductId == null
		    || fb.Quantity == null)
			return false;
		
		bool? result = DbQuery.SetQuantityOfProducts(fb.ProductId ??= 0, fb.Quantity ??= 0, fb.UserId);
		return result ??= false;
	}

	private bool RemoveProduct(FrontBack fb)
	{
		if (fb == null
		    || fb.ProductId == null)
			return false;
		return userApi.RemoveProduct(fb.ProductId ??= 0, fb.UserId);
	}

	private bool FindProduct(FrontBack fb)
	{
		if (fb == null
		    || fb.ProductId == null)
			return false;
		return userApi.NumberOfProduct(fb.ProductId ?? 0, fb.UserId) > 0;
	}

	private List<Product> GetProducts(FrontBack fb)
	{
		if (fb == null)
			return new();

		List<Product>? x = DbQuery.GetUserOwnedProducts(fb.UserId);
		if (x == null)
			return new();
		
		return x;
	}

	private bool AddProductsToShoppingList(FrontBack fb)
	{
		if (fb == null
		    || fb.ProductId == null)
			return false;

		fb.Quantity ??= 1;
		return userApi!.AddProductToShoppingList(fb.ProductId ??= 0, fb.Quantity ??= 0, fb.UserId);
	}

	private bool RemoveProductsFromShoppingList(FrontBack fb)
	{
		if (fb == null
		    || fb.ProductId == null)
			return false;
		return userApi.RemoveProduct(fb.ProductId ?? 0, fb.UserId);
	}

	private bool AddProduct(FrontBack fb)
	{
		if (fb == null)
			return false;

		fb.Quantity ??= 1;
		int? pid;
		
		if (fb.Barcode is not null)
		{
			if (string.IsNullOrEmpty(fb.Barcode))
				return false;
			else
			{
				Product? product = DbQuery.GetProductFromDB(fb.Barcode);
				if (product == null)
					return false;
				pid = product.Id;
			}
		}
		else if (fb.ProductId is not null)
			pid = fb.ProductId;
		else pid = 0;
		
		return DbQuery.AddProduct((int)pid, fb.Quantity ??= 0, fb.UserId);
	}
}