-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Mar 2022, 00:38
-- Wersja serwera: 10.4.22-MariaDB
-- Wersja PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `smart_shopping`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products`
--

CREATE TABLE `ss_products` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `barcode` int(11) NOT NULL,
  `img_url` text NOT NULL,
  `waste_type` int(11) NOT NULL DEFAULT 1,
  `product_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_consumption`
--

CREATE TABLE `ss_products_consumption` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_ingredients`
--

CREATE TABLE `ss_products_ingredients` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_ingredients_in_products`
--

CREATE TABLE `ss_products_ingredients_in_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_nutritional_values`
--

CREATE TABLE `ss_products_nutritional_values` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_nutritional_values_in_products`
--

CREATE TABLE `ss_products_nutritional_values_in_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `nv_id` int(11) NOT NULL,
  `q_per_100` double NOT NULL,
  `q_per_package` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_owned_by_user`
--

CREATE TABLE `ss_products_owned_by_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_types`
--

CREATE TABLE `ss_products_types` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_products_waste_types`
--

CREATE TABLE `ss_products_waste_types` (
  `id` int(11) NOT NULL,
  `name_pl` text NOT NULL,
  `name_eng` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ss_products_waste_types`
--

INSERT INTO `ss_products_waste_types` (`id`, `name_pl`, `name_eng`) VALUES
(1, 'mieszane', 'mixed'),
(2, 'plastik', 'plastic'),
(3, 'papier', 'paper'),
(4, 'szkło', 'glass');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_recipes_names`
--

CREATE TABLE `ss_recipes_names` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `author` text NOT NULL,
  `img_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_recipes_products_list`
--

CREATE TABLE `ss_recipes_products_list` (
  `id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_shopping_list`
--

CREATE TABLE `ss_shopping_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ss_user`
--

CREATE TABLE `ss_user` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ss_user`
--

INSERT INTO `ss_user` (`id`, `name`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `ss_products`
--
ALTER TABLE `ss_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `waste_type` (`waste_type`),
  ADD KEY `product_type` (`product_type`);

--
-- Indeksy dla tabeli `ss_products_consumption`
--
ALTER TABLE `ss_products_consumption`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeksy dla tabeli `ss_products_ingredients`
--
ALTER TABLE `ss_products_ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ss_products_ingredients_in_products`
--
ALTER TABLE `ss_products_ingredients_in_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `ingredient_id` (`ingredient_id`);

--
-- Indeksy dla tabeli `ss_products_nutritional_values`
--
ALTER TABLE `ss_products_nutritional_values`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ss_products_nutritional_values_in_products`
--
ALTER TABLE `ss_products_nutritional_values_in_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `nv_id` (`nv_id`);

--
-- Indeksy dla tabeli `ss_products_owned_by_user`
--
ALTER TABLE `ss_products_owned_by_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeksy dla tabeli `ss_products_types`
--
ALTER TABLE `ss_products_types`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ss_products_waste_types`
--
ALTER TABLE `ss_products_waste_types`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ss_recipes_names`
--
ALTER TABLE `ss_recipes_names`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ss_recipes_products_list`
--
ALTER TABLE `ss_recipes_products_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeksy dla tabeli `ss_shopping_list`
--
ALTER TABLE `ss_shopping_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indeksy dla tabeli `ss_user`
--
ALTER TABLE `ss_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `ss_products`
--
ALTER TABLE `ss_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_consumption`
--
ALTER TABLE `ss_products_consumption`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_ingredients`
--
ALTER TABLE `ss_products_ingredients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_ingredients_in_products`
--
ALTER TABLE `ss_products_ingredients_in_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_nutritional_values`
--
ALTER TABLE `ss_products_nutritional_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_nutritional_values_in_products`
--
ALTER TABLE `ss_products_nutritional_values_in_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_owned_by_user`
--
ALTER TABLE `ss_products_owned_by_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_types`
--
ALTER TABLE `ss_products_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_products_waste_types`
--
ALTER TABLE `ss_products_waste_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `ss_recipes_names`
--
ALTER TABLE `ss_recipes_names`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_recipes_products_list`
--
ALTER TABLE `ss_recipes_products_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_shopping_list`
--
ALTER TABLE `ss_shopping_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ss_user`
--
ALTER TABLE `ss_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `ss_products`
--
ALTER TABLE `ss_products`
  ADD CONSTRAINT `ss_products_ibfk_1` FOREIGN KEY (`waste_type`) REFERENCES `ss_products_waste_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_products_ibfk_2` FOREIGN KEY (`product_type`) REFERENCES `ss_products_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ss_products_consumption`
--
ALTER TABLE `ss_products_consumption`
  ADD CONSTRAINT `ss_products_consumption_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ss_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_products_consumption_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `ss_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ss_products_ingredients_in_products`
--
ALTER TABLE `ss_products_ingredients_in_products`
  ADD CONSTRAINT `ss_products_ingredients_in_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `ss_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_products_ingredients_in_products_ibfk_2` FOREIGN KEY (`ingredient_id`) REFERENCES `ss_products_ingredients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ss_products_nutritional_values_in_products`
--
ALTER TABLE `ss_products_nutritional_values_in_products`
  ADD CONSTRAINT `ss_products_nutritional_values_in_products_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `ss_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_products_nutritional_values_in_products_ibfk_2` FOREIGN KEY (`nv_id`) REFERENCES `ss_products_nutritional_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ss_products_owned_by_user`
--
ALTER TABLE `ss_products_owned_by_user`
  ADD CONSTRAINT `ss_products_owned_by_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ss_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_products_owned_by_user_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `ss_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ss_recipes_products_list`
--
ALTER TABLE `ss_recipes_products_list`
  ADD CONSTRAINT `ss_recipes_products_list_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `ss_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_recipes_products_list_ibfk_2` FOREIGN KEY (`recipe_id`) REFERENCES `ss_recipes_names` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ss_shopping_list`
--
ALTER TABLE `ss_shopping_list`
  ADD CONSTRAINT `ss_shopping_list_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ss_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ss_shopping_list_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `ss_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
