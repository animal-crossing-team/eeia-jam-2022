import { React, useEffect, useState } from 'react'
import axios from "axios";
import { useParams } from 'react-router-dom';

export default function Magazine(params) {
	let { id } = useParams()
	useEffect(() => {
		getData()
	}, []);

	const [products, setProducts] = useState([]);
	const getData = async () => {
		const response = await axios.post(
			`http://25.45.58.193:5094/magazine/get`,
			{
				userId: id
			},
			{ headers: { "Content-Type": "application/json" } }
		);
		console.log(response)
		setProducts(response.data)
	}
	const getName = async (barcode) => {
		let response = await axios.get(`https://world.openfoodfacts.org/api/v2/product/${barcode}`)
		console.log('response', response)
        return response.data.product.product_name
	} 
	return (
		<div className="magazine">
			<h2>Produkty dostępne w Twojej spiżarce:</h2>
			{
				products.map(element => {
					return (
						<div className="element">
							<div className="right">
								{element.name ? element.name : () => {
									getName(element.barcode)
								}}
							</div>
							<div className="left">
								{element.quantity}
							</div>
						</div>
					)
				})
			}
		</div>
	)
}
