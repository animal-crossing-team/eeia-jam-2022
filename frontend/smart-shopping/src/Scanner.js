import React from 'react'
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import axios from "axios";
import { useState } from 'react'
import WebCam from './WebCam';
import zaklin from './zaklin.png'
import { useParams } from 'react-router-dom';

export default function Scanner(params) {
    let { id } = useParams()
    const [result, setResult] = useState(null);

    const onDetected = result => {
        setResult(result);
        console.log(result)
        document.getElementById('barcode').value = result
    };

    const [productDetails, setProductDetails] = useState({});

    const check_barcode = async () => {
        let input = document.getElementById('barcode')
        let response = await axios.get(`https://world.openfoodfacts.org/api/v2/product/${input.value}`)
        console.log(response.data.product)
        setProductDetails(response.data.product)
    }

    const handleAdd = async () => {
        const response = await axios.post(
			`http://25.45.58.193:5094/magazine/add`,
			{
				userId: id,
                barcode: result
			},
			{ headers: { "Content-Type": "application/json" } }
		);
		console.log(response)
        if (response.data) 
            window.alert('Product added!')
        else window.alert('Something went wrong!')
    }
    return (
        <div className='trash-bin'>
            <div className="scanner">
                <h1>Skaner produktów</h1>
                <div className='web-cam'>
                    <WebCam onDetected={onDetected} />
                </div>
                <div className="search-bar-code">
                    <TextField
                        margin="normal"
                        fullWidth
                        id="barcode"
                        label="Kod kreskowy produktu"
                        name="barcode"
                        autoComplete="barcode"
                        focused
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        onClick={check_barcode}
                    >
                        Skanuj kod kreskowy
                    </Button>
                    <Button
                        variant="contained"
                        color="success"
                        fullWidth
                        style={{ marginTop: 50, fontSize: 28 }}
                        onClick={handleAdd}
                    >
                        Dodaj produkt
                    </Button>
                </div>
            </div>
            <div className="bins">
                <div className="product-details">
                    <h3>Szczegóły produktu:</h3>
                    <strong>Nazwa: </strong>{productDetails.product_name}<br />
                    <strong>Ilość: </strong>{productDetails.quantity}<br />
                </div>
                <img src={zaklin} alt="zaklin" width="80%" style={{ marginTop: '100px', marginLeft: '10%', imageRendering: 'pixelated' }} /><br /><br /><br />
                <div style={{ width: '100%', textAlign: 'center' }}>

                    <i>Żaklin spogląda czy zdrowo się odżywiasz</i>
                </div>
            </div>
        </div>
    )
}
