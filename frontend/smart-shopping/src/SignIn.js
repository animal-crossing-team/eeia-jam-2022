import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import axios from "axios";
import { Link as RootLink, useNavigate } from "react-router-dom";
const theme = createTheme();

export default function SignIn() {
	let navigate = useNavigate();
	const redirect = (id) => {
		navigate(`/user/${id}`)
	};
	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		// eslint-disable-next-line no-console
		console.log({
			nick: data.get('nick'),
			password: data.get('password'),
		});
		const response = await axios.post(
			`http://25.45.58.193:5094/user/login`,
			{
				login: data.get('nick'),
				password: data.get('password'),
			},
			{ headers: { "Content-Type": "application/json" } }
		);
		console.log(response)
		if (response.data > 0) {
			redirect(response.data)
		}
		else window.alert('Podane hasło i/lub nazwa użytkownika są niepoprawne!')
	};

	return (
		<ThemeProvider theme={theme}>
			<Container component="main" maxWidth="xs">
				<CssBaseline />
				<Box
					sx={{
						marginTop: 8,
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
					}}
				>
					<Typography component="h1" variant="h2" style={{ padding: '100px 0', textAlign: 'center' }}>
						SMART SHOPPING
					</Typography>
					<Typography component="h2" variant="h5">
						Sign in
					</Typography>
					<Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
						<TextField
							margin="normal"
							required
							fullWidth
							id="nick"
							label="Nickname"
							name="nick"
							autoComplete="nick"
							autoFocus
						/>
						<TextField
							margin="normal"
							required
							fullWidth
							name="password"
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
						/>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							sx={{ mt: 3, mb: 2 }}
						>
							Sign In
						</Button>
						<Grid container>
							<Grid item xs>
							</Grid>
							<Grid item>
								<Link variant="body2">
									<RootLink to="/signup">
										{"Don't have an account? Sign Up"}
									</RootLink>
								</Link>
							</Grid>
						</Grid>
					</Box>
				</Box>
			</Container>
		</ThemeProvider>
	);
}