import React from 'react'
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import axios from "axios";
import { useState } from 'react'
import WebCam from './WebCam';

export default function TrashBin() {
    const [productDetails, setProductDetails] = useState({});

    const check_barcode = async () => {
        let input = document.getElementById('barcode')
        let response = await axios.get(`https://world.openfoodfacts.org/api/v2/product/${input.value}`)
        console.log(response.data.product)
        setProductDetails(response.data.product)
        let xd = document.getElementsByClassName('bin')
        xd = Array.from(xd)
        xd.forEach(element => {
            if (element.classList.length > 1) {
                let to_remove = element.classList[1]
                element.classList.remove(to_remove)
            }
        });
        if (response.data.product.packaging_tags.includes("en:plastic"))
            document.getElementById('plastic').classList.add('bin-plastic')
        else if (response.data.product.packaging_tags.includes("en:glass"))
            document.getElementById('glass').classList.add('bin-glass')
        else
            document.getElementById('mix').classList.add('bin-mix')
    }
    const throw_trash = () => {
        let xd = document.getElementsByClassName('bin')
        xd = Array.from(xd)
        xd.forEach(element => {
            if (element.classList.length > 1) {
                let to_remove = element.classList[1]
                element.classList.remove(to_remove)
            }
        });
        setProductDetails({})
    }
    const [result, setResult] = useState(null);

    const onDetected = result => {
        setResult(result);
        console.log(result)
        document.getElementById('barcode').value = result
    };

    return (
        <div className='trash-bin'>
            <div className="scanner">
                <h1>Kosz</h1>
                <div className='web-cam'>
                    <WebCam onDetected={onDetected} />
                </div>
                <div className="search-bar-code">
                    <TextField
                        margin="normal"
                        fullWidth
                        id="barcode"
                        label="Kod kreskowy produktu"
                        name="barcode"
                        autoComplete="barcode"
                        focused
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        onClick={check_barcode}
                    >
                        Skanuj kod kreskowy
                    </Button>
                    <Button
                        variant="contained"
                        color="error"
                        fullWidth
                        style={{ marginTop: 20, fontSize: 28 }}
                        onClick={throw_trash}
                    >
                        Wyrzuć
                    </Button>
                </div>
            </div>
            <div className="bins">
                <div className="product-details">
                    <h3>Szczegóły produktu:</h3>
                    <strong>Nazwa: </strong>{productDetails.product_name}<br />
                    <strong>Ilość: </strong>{productDetails.quantity}<br />
                    <strong>Typ opakowania: </strong>{productDetails.packaging}<br />
                </div>
                <div className="bin" id='bio' onClick={() => document.getElementById('bio').classList.add('bin-bio')}>BIO</div>
                <div className="bin" id='plastic' onClick={() => document.getElementById('plastic').classList.add('bin-plastic')}>TWORZYWA</div>
                <div className="bin" id='glass' onClick={() => document.getElementById('glass').classList.add('bin-glass')}>SZKŁO</div>
                <div className="bin" id='paper' onClick={() => document.getElementById('paper').classList.add('bin-paper')}>PAPIER</div>
                <div className="bin" id='mix' onClick={() => document.getElementById('mix').classList.add('bin-mix')}>MIX</div>
            </div>
        </div>
    )
}
