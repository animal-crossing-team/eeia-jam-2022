import React from 'react'
import { useState } from 'react'
import TrashBin from './TrashBin';
import List from './List';
import Magazine from './Magazine';
import Scanner from './Scanner';
export default function User() {
	const [showSkanowanie, setShowSkanowanie] = useState(true);
	const [showMagazyn, setShowMagazyn] = useState(false);
	const [showLista, setShowLista] = useState(false);
	const [showKosz, setShowKosz] = useState(false);

	const show_panel = (panelName) => {
		setShowKosz(false)
		setShowLista(false)
		setShowMagazyn(false)
		setShowSkanowanie(false)
		document.getElementById('li1').style.fontWeight = 400
		document.getElementById('li2').style.fontWeight = 400
		document.getElementById('li3').style.fontWeight = 400
		document.getElementById('li5').style.fontWeight = 400
		switch (panelName) {
			case 'kosz':
				setShowKosz(true)
				document.getElementById('li5').style.fontWeight = 700
				break
			case 'lista':
				setShowLista(true)
				document.getElementById('li3').style.fontWeight = 700
				break
			case 'magazyn':
				setShowMagazyn(true)
				document.getElementById('li2').style.fontWeight = 700
				break
			case 'skanowanie':
				setShowSkanowanie(true)
				document.getElementById('li1').style.fontWeight = 700
				break
		}
	}
	return (
		<div className="user-container">
			<div className="left-panel">
				<h1>SMART SHOPPING</h1>
				<ul className="panels">
					<li id='li1' onClick={() => show_panel('skanowanie')} style={{fontWeight: '700'}}>Skanowanie</li>
					<li id='li2' onClick={() => show_panel('magazyn')}>Spiżarka</li>
					<li id='li3' onClick={() => show_panel('lista')}>Lista zakupów</li>
					<li id='li5' onClick={() => show_panel('kosz')}>Kosz</li>
				</ul>
			</div>
			<div className="right-panel">
				{showKosz ? <TrashBin /> : null}
				{showLista ? <List /> : null}
				{showMagazyn ? <Magazine /> : null}
				{showSkanowanie ? <Scanner /> : null}
			</div>
		</div>
	)
}
